# xmlobjecteditor
Command line editor for XML files

Usage:
python xmlobjecteditor.py <options> --file="<file>"
Options:
--element= - (Required, Default=NULL)
Full path to the element being modified, starting at the root element. Nested objects are seperated by a period.
--value= - (Required, Default=NULL)
Value to set the element  to.
--file= - (Required, Default=NULL)
--element-attr - (Optional, Default=NULL)
Only modify elements with this attribute and value. Seperate the attribute and value with a colon.
Format: attribute:value Example: --attribute="name:Catalina". Can be specified multiple times.
--attribute= - (Optional, Default=NULL)
New value of the element or attribute. 
-V,--verbose - (Optional, Default=False)
Run in verbose mode. Debugging output to shell.
--dry-run - (Optional, Default=False)
Do not actually make the edits, only show what would have been done.
--fail - (Optional, Default=False)
Return a non-zero (2) return value if the element is not found. The default is to return 0.
--backup= (Optional, Default=False,".bak")
Create a backup of the file before making changes. The value will be used as the saved file's extension.
TODO:
-r --regex (Optional, Default=NULL)
Instead of a single value, perform a regex search and replace on the element's value.