#!/usr/bin/env python
"""
xmlobjecteditor.py

Created by Russell Cook<rcook@64byte.com> on 06/23/2018.
License: The Unlincense (http://unlicense.org/)
https://github.com/bz0qyz/xmlobjecteditor/


Changelog:

"""
###
## Import Modules
###
import os
import sys
import argparse
import xml.etree.ElementTree as ElementTree
from pprint import pprint


###
## Global Variables
###
## Name of this application
app_name = 'xmlobjecteditor'
## Version of this application
app_version = '1.0.0'
## Description of this application
app_description = 'Simple command-line XML file editor'
## Dynamic path to this application directory
app_path = os.path.dirname(os.path.realpath(__file__))

###
## Constants
###
# The dilimeter used in arguments to seperate attributes and values
ATTR_DILIMETER=':'

###
## Process command-line arguments
###
parser = argparse.ArgumentParser(description=app_name + " Usage:")
parser.add_argument('--action',  metavar='action', default="auto", required=False, choices=['add','modify','auto'], help='Action to perform. Actions: add, modify, auto. Default: auto (Modify if element exists, otherwise add)')
parser.add_argument('--file',  metavar='xmlfile', default="", required=True, help='Full path to XML File to modify.')
parser.add_argument('--element',  metavar='element', default="", required=True, help='Full path to the element being modified, starting at the root element. Nested objects are seperated by a comma')
parser.add_argument('--element-attr',  metavar='element-attr', default=[], nargs='*', help='Only modify elements with these attribute and value. Seperate the attribute and value with a: "' + ATTR_DILIMETER + '".')
parser.add_argument('--attribute',  metavar='attribute', default="", help='Element attribute to modify instead of the element\'s value.')
parser.add_argument('--value',  metavar='value', default="", required=True, help='New value of the element or attribute.')
parser.add_argument('--version', action="version", default=False, version=app_name + ' ' + app_version )
parser.add_argument('-v','--verbose', action="count", default=False, help='Show verbose output.')
parser.add_argument('--dry-run', action="store_true", default=False, help='Do not actually make the edits, only show what would have been done.')
parser.add_argument('--fail', action="store_true", default=False, help='Return a non-zero (2) return value if the element is not found. The default is to return 0.')
parser.add_argument('--backup',  metavar='backup', help='Create a backup of the file before making changes. The value will be used as the backup file\'s extension.')
#parser.add_argument('--regex',  metavar='regex', help='Instead of a single value, perform a regex search and replace on the element\'s value.')
args = parser.parse_args()

###
## Global variable overrides
###
verbose = args.verbose

###
############ INTERNAL FUNCTIONS ##################
###
def show_version():
	print(app_name + " Version: " + app_version)
	print(app_description)
	sys.exit(0)

def element_attr_match(elem,attribute_from_args=[]):
	global ATTR_DILIMETER
	mynut = 10
	if len(attribute_from_args) > 0:
		aAttr = attribute_from_args.split(ATTR_DILIMETER)
		if elm.attrib[aAttr[0]] == aAttr[1]:
			#print(aAttr[0], aAttr[1])
			return True
		else:
			return False
	else:
		return False

###
############ MAIN FUNCTION ##################
###
if __name__ == "__main__":
	
	if verbose > 2:
		print('DEBUG: Arguments: ')
		pprint(args)
		print('verbose is ?', str(verbose))

	# Check if the XML file exists
	if not os.path.isfile(args.file):
		print('\nFATAL: Unable to locate file: '  + args.file)
		
	# Attempt to parse the XML File
	try:
		tree = ElementTree.parse(args.file)
		root = tree.getroot()
		#print(root.attrib)
	except ValueError as e:
		print('FATAL: unable to parse XML file.  Details: ' + str(e))

	# Find the specified element(s) in the XML tree
	elements_found=[]
	
	if verbose > 0:
		print("Searching for elements named: " + args.element)
	
	try:
		for elm in  root.iter(args.element):
			
			# If we have element attributes specified, make sure our located element matches
			if len(args.element_attr) > 0:
				for attr in args.element_attr:
					if element_attr_match(elm,attr):
						print(elm.tag,elm.attrib)
			
	except ValueError as e:
		print("Error locating element: " + args.element)
	

	#for neighbor in root.iter('neighbor'):
	#	pprint(neighbor)

	#if verbose > 1:
	#	print("Searching for element named: " + args.element) 
	#for element in root.findall(args.element):
	#	pprint(element)